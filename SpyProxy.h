#ifndef SpyProxy_H
#define SpyProxy_H

#include <QObject>
#include <QUdpSocket>

class SpyProxy : public QObject
{
    Q_OBJECT

public:
    explicit SpyProxy(QObject *parent = nullptr);

private Q_SLOTS:
    void readPhone();
    void readDrone();

private:
    QUdpSocket m_phoneSocket;
    QUdpSocket m_droneSocket;

    QHostAddress m_phoneAddr;
    QHostAddress m_droneAddr;
    quint16 m_phonePort = 0;
};

#endif // UDPREADER_H
