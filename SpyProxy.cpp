#include "SpyProxy.h"
#include <QDebug>
#include <QNetworkInterface>

SpyProxy::SpyProxy(QObject *parent) : QObject(parent)
{
    connect(&m_phoneSocket, &QUdpSocket::readyRead, this, &SpyProxy::readPhone);
    m_phoneSocket.bind(QHostAddress::Any, 8080, QUdpSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);

    connect(&m_droneSocket, &QUdpSocket::readyRead, this, &SpyProxy::readDrone);
    m_droneSocket.bind(QHostAddress::Any, 23174, QUdpSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);
}


void SpyProxy::readPhone()
{
    QByteArray datagram;

    while (m_phoneSocket.hasPendingDatagrams()) {
        datagram.resize((int)m_phoneSocket.pendingDatagramSize());
        m_phoneSocket.readDatagram(datagram.data(), datagram.size(), &m_phoneAddr, &m_phonePort);

        //if ((unsigned char)datagram[0] != 0xFF) {
            qDebug() << "P->D" << datagram.toHex(' ') << datagram;
        //}

        // Calculate the drone IP
        if (m_droneAddr.isNull()) {
            for (const QNetworkInterface &interf: QNetworkInterface::allInterfaces()) {
                if (interf.type() == QNetworkInterface::Loopback) {
                    continue;
                }
                for (const QNetworkAddressEntry &addr: interf.addressEntries()) {
                    if (addr.ip().protocol() != QAbstractSocket::IPv4Protocol) {
                        continue;
                    }
                    if (addr.ip() == m_phoneAddr) {
                        continue;
                    }
                    quint32 intaddr = addr.ip().toIPv4Address();
                    intaddr &= 0xFFFFFF00;
                    intaddr |= 0x01;
                    m_droneAddr = QHostAddress(intaddr);
                }
            }
        }
        if (!m_droneAddr.isNull()) {
            m_droneSocket.writeDatagram(datagram, m_droneAddr, 8080);
        }
        else {
            qDebug() << "No drone found";
        }
    }
}


void SpyProxy::readDrone()
{
    QByteArray datagram;
    quint16 port;

    while (m_droneSocket.hasPendingDatagrams()) {
        datagram.resize((int)m_droneSocket.pendingDatagramSize());
        m_droneSocket.readDatagram(datagram.data(), datagram.size(), &m_droneAddr, &port);
        qDebug() << "D->P" << datagram.toHex(' ') << datagram;
        m_phoneSocket.writeDatagram(datagram, m_phoneAddr, m_phonePort);
    }
}

