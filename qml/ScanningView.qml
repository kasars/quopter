import QtQuick 2.3
import QtQuick.Controls 1.4

Rectangle {
    id: scanView
    property alias model: listView.model

    property bool scanning: true

    signal selectItem(int index)
    signal reScan()

    color: "white"
    border.width: 2
    border.color: "#A0A0A0"

    MouseArea {
        anchors.fill: parent
    }

    ScrollView {
        anchors.fill: parent
        anchors.margins: 2
        ListView {
            id: listView

            delegate: Rectangle {
                height: scanView.height/6
                width: scanView.width
                color: mArea.pressed ? "#C0C0C0" : "white"
                Rectangle {
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    height: 1
                    color: "#A0A0A0"
                    visible: index > 0
                }
                Text {
                    id: textItem
                    anchors.fill: parent

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                    font.pixelSize: parent.height / 3
                    text: modelData
                }
                MouseArea {
                    id: mArea
                    anchors.fill: parent
                    onClicked: {
                        scanView.selectItem(index);
                    }
                }
            }
        }
    }


    Text {
        id: statusText
        anchors.centerIn: parent
        font.pixelSize: scanView.height / 10
        text: scanning ? qsTr("Scanning for devices...") : qsTr("No devices found.")
        visible: scanning || listView.count == 0
    }

    JoyPadButton {
        anchors {
            top: statusText.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: parent.height/10
        }
        font.pixelSize: height/4
        text: qsTr("Restart device scan")

        onPressedChanged: {
            if (pressed) {
                scanView.reScan();
            }
        }
        visible: !scanning && listView.count == 0
    }
}
