import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    width: 1376
    height: 786
    title: qsTr("JoyPad")

    Rectangle {
        anchors.fill: parent
        color: "#888888"
    }

    Image {
        anchors.centerIn: parent
        source: "Drone.png"
        sourceSize.width: parent.width * 0.25
        sourceSize.height: parent.width * 0.25
        rotation: -10
    }

    Item {
        anchors.fill: parent
        opacity: 1

        focus: true

        Row {
            anchors {
                top: parent.top
                left: parent.left
                right: leftStick.right
                bottom: leftStick.top
            }

            JoyPadButton {
                id: btn1
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: ""

            }
            JoyPadButton {
                id: btn2
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: ""
            }
        }

        Joystick {
            id: leftStick;
            anchors {
                left: parent.left
                verticalCenter: parent.verticalCenter
            }
            width: parent.width*0.35
            height: width
            useTurnArrows: true
            useGyroscope: true

            onXPossChanged: ctrlWriter.yaw = xPoss
            onYPossChanged: ctrlWriter.uplift = -yPoss
        }

        Row {
            anchors {
                top: leftStick.bottom
                left: parent.left
                right: leftStick.right
                bottom: parent.bottom
            }

            JoyPadButton {
                id: btn3
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: qsTr("Arm")
                activity: ctrlWriter.armPressed
                onPressedChanged: if (pressed) ctrlWriter.handleArm()
            }
            JoyPadButton {
                id: btn4
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: qsTr("Disarm")
                activity: ctrlWriter.disarmPressed
                onPressedChanged: if (pressed) ctrlWriter.handleDisarm();
            }
        }

        Row {
            anchors {
                top: parent.top
                left: rightStick.left
                right: rightStick.right
                bottom: rightStick.top
            }

            JoyPadButton {
                id: btn5
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: qsTr("")
                //activity: false
                //onPressedChanged:
            }
            JoyPadButton {
                id: btn6
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: qsTr("Cal");
                activity: ctrlWriter.calibrPressed
                onPressedChanged: if (pressed) ctrlWriter.handleCalibrate();
            }
        }


        Joystick {
            id: rightStick;
            anchors {
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            width: parent.width*0.35
            height: width
            constantRadius: true
            useAccelerometer: false

            onXPossChanged: ctrlWriter.roll = xPoss
            onYPossChanged: ctrlWriter.pitch = -yPoss
        }

        Row {
            anchors {
                top: rightStick.bottom
                left: rightStick.left
                right: parent.right
                bottom: parent.bottom
            }

            JoyPadButton {
                id: btn7
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: "Land"
                activity: ctrlWriter.landPressed
                onPressedChanged: if (pressed) ctrlWriter.handleLand();
            }
            JoyPadButton {
                id: btn8
                height: parent.height
                width: parent.width/2
                font.pixelSize: height/4
                text: Qt.platform.os === "android" ? qsTr("Arm\nDisarm") : qsTr("Disarm")
                activity: ctrlWriter.disarmPressed || ctrlWriter.armPressed
                onPressedChanged: {
                    if (pressed) {
                        ctrlWriter.handleArm();
                    }
                    else {
                        ctrlWriter.handleDisarm();
                    }
                }
            }
            Component.onCompleted: {
                console.log("Qt.platform.os", Qt.platform.os);
            }
        }

        Keys.onPressed: {
            if (event.key === Qt.Key_Back || event.key === Qt.Key_Escape || event.key === Qt.Key_Space) {
                ctrlWriter.handleDisarm();
                event.accepted = true;
            }
            if (event.key === Qt.Key_W) {
                ctrlWriter.uplift = 0.5;
                event.accepted = true;
            }
            if (event.key === Qt.Key_S) {
                ctrlWriter.uplift = -0.5;
                event.accepted = true;
            }
            if (event.key === Qt.Key_A) {
                ctrlWriter.yaw = -0.5;
                event.accepted = true;
            }
            if (event.key === Qt.Key_D) {
                ctrlWriter.yaw = 0.5;
                event.accepted = true;
            }
        }
        Keys.onReleased: {
            if (event.key === Qt.Key_Back || event.key === Qt.Key_Escape) {
                event.accepted = true;
            }
            if (event.key === Qt.Key_W) {
                ctrlWriter.uplift = 0;
                event.accepted = true;
            }
            if (event.key === Qt.Key_S) {
                ctrlWriter.uplift = 0;
                event.accepted = true;
            }
            if (event.key === Qt.Key_A) {
                ctrlWriter.yaw = 0;
                event.accepted = true;
            }
            if (event.key === Qt.Key_D) {
                ctrlWriter.yaw = 0;
                event.accepted = true;
            }
        }
    }
}
