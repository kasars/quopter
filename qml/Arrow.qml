import QtQuick 2.3

Item {
    id: arrow
    property double lineWidth: 2
    property color color: "white"

    property bool leftTurn: false
    property bool rightTurn: false


    implicitHeight: 40
    implicitWidth: 40

    clip: true

    Rectangle {
        id: leftLine
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.top
        }
        height: lineWidth
        width: arrow.width
        rotation: -45
        color: arrow.color
    }

    Rectangle {
        id: rightLine
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.top
        }
        height: lineWidth
        width: arrow.width
        rotation: 45
        color: arrow.color
    }

    Rectangle {
        id: leftTurnItem
        anchors {
            horizontalCenter: parent.left
            horizontalCenterOffset: lineWidth/2
            top: parent.top
            topMargin: -(lineWidth + radius)
        }
        height: parent.height
        width: parent.width
        color: "#00000000"
        border.width: lineWidth
        border.color: arrow.color
        visible: leftTurn
        radius: width/5
    }

    Rectangle {
        id: rightTurnItem
        anchors {
            horizontalCenter: parent.right
            horizontalCenterOffset: -lineWidth/2
            top: parent.top
            topMargin: -(lineWidth + radius)
        }
        height: parent.height
        width: parent.width
        color: "#00000000"
        border.width: lineWidth
        border.color: arrow.color
        visible: rightTurn
        radius: width/5
    }
}
