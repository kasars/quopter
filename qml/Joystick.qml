import QtQuick 2.3
import QtSensors 5.11

Rectangle {
    id: pad
    implicitHeight: 200
    implicitWidth: 200
    radius: constantRadius ? width/2 : stick.radius
    color: "#00000000" // "#A0A0A0"
    border.color: "#FFFFFF"
    border.width: lineWidths
    property alias stickColor: stick.color

    property bool constantRadius: false
    property bool useAccelerometer: false
    property bool useGyroscope: false

    property bool useTurnArrows: false

    property double margins: height * 0.1
    property double lineWidths: height * 0.01

    readonly property real xPoss: centerX !== 0 ? (stick.x - centerX)/centerX : 0
    readonly property real yPoss: centerY !== 0 ? (stick.y - centerY)/centerY : 0

    readonly property bool accelAvailable: accel.accelAvailable
    readonly property bool gyroAvailable: gyro.gyroAvailable

    Arrow {
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: margins
        }
        width: margins * 2
        height: margins * 2
        lineWidth: lineWidths
    }

    Arrow {
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: margins
        }
        rotation: 180
        width: margins * 2
        height: margins * 2
        lineWidth: lineWidths
    }

    Arrow {
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            margins: margins
        }
        width: margins * 2
        height: margins * 2
        lineWidth: lineWidths
        rotation: 270

        leftTurn: useTurnArrows
    }

    Arrow {
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            margins: margins
        }
        rotation: 90
        width: margins * 2
        height: margins * 2
        lineWidth: lineWidths

        rightTurn: useTurnArrows
    }


    Rectangle {
        id: stick
        width: pad.width * 0.25
        height: pad.height * 0.25
        radius: width/2
        x: centerX
        y: centerY

        color: "white"
        border.color: "black"
        border.width: 2
        NumberAnimation {
            id: centerXAnimation;
            running: false
            target: stick
            property: "x"
            to: centerX
            duration: 100
        }
        NumberAnimation {
            id: centerYAnimation;
            running: false
            target: stick
            property: "y"
            to: centerY
            duration: 100
        }

        Rectangle {
            anchors.centerIn: parent
            height: parent.height*0.90
            color: "#808080"
            border.color: parent.border.color
            border.width: 2
            width: height
            radius: width/2
        }
    }

    readonly property double centerX: pad.width/2 - stick.width/2
    readonly property double centerY: pad.height/2 - stick.height/2

    MultiPointTouchArea {
        anchors.fill: parent
        minimumTouchPoints: 1
        maximumTouchPoints: 1

        visible: !(useAccelerometer && accel.accelAvailable)

        touchPoints: [
            TouchPoint {
                id: touch1
                property real lastX: -1
                property real lastY: -1

                onPressedChanged: {
                    if (!pressed) {
                        if (!(useGyroscope && gyro.gyroAvailable)) {
                            centerXAnimation.running = true;
                        }
                        centerYAnimation.running = true;
                    }
                    else {
                        lastX = -1;
                        lastY = -1;
                    }
                }
            }
        ]

        onTouchUpdated: {
            if (touch1.lastX == -1) touch1.lastX = touch1.x;
            var newX = stick.x - (touch1.lastX - touch1.x);
            newX = Math.min(Math.max(0, newX), pad.width-stick.height);
            touch1.lastX = touch1.x;

            if (touch1.lastY == -1) touch1.lastY = touch1.y;
            var newY = stick.y - (touch1.lastY - touch1.y);
            newY = Math.min(Math.max(0, newY), pad.height-stick.height);
            touch1.lastY = touch1.y;

            if (constantRadius) {
                var xAmpl = newX - centerX;
                var yAmpl = newY - centerY;
                var length = Math.sqrt(xAmpl * xAmpl + yAmpl * yAmpl);
                if (length > centerX) {
                    xAmpl *= centerX/length;
                    yAmpl *= centerY/length;
                    newX = xAmpl + centerX;
                    newY = yAmpl + centerY;
                }
            }
            if (!(useGyroscope && gyro.gyroAvailable)) {
                stick.x = newX;
            }
            stick.y = newY;
        }
    }

    Accelerometer {
        id: accel
        dataRate: 100
        active: useAccelerometer

        property bool accelAvailable: true

        onReadingChanged: {
            if (!accelAvailable) return;
            var x = accel.reading.y;
            var y = accel.reading.x;
            var z = accel.reading.z;
            var length = Math.sqrt(x * x + z * z + y * y);
            var newX = x * centerX/length + centerX;
            var newY = y * centerY/length + centerY;
            stick.x = newX;
            stick.y = newY;
        }
    }

    Gyroscope {
        id: gyro
        dataRate: 1
        active: useGyroscope

        property bool gyroAvailable: true

        onReadingChanged: {
            if (!gyroAvailable) return;
            var z = reading.z/100;
            if (Math.abs(z) > 1) {
                z /= Math.abs(z);
            }
            stick.x = centerX - z * centerX;
        }
    }


    Component.onCompleted: {
        var types = QmlSensors.sensorTypes();
        console.log(types.join(", "));
        if (types.indexOf("QAccelerometer") === -1) {
            accel.accelAvailable = false;
        }
        if (types.indexOf("QGyroscope") === -1) {
            gyro.gyroAvailable = false;
        }
    }
}
