#!/bin/sh

# mkdir build_android
# cd build_android
# ../configure_android_qt5.15.sh

export ANDROID_API=30
export ANDROID_HOME=/opt/Android/android-sdk
export ANDROID_NDK=/opt/Android/android-sdk/ndk/22.1.7171670

export CMAKE_PREFIX_PATH=/opt/Qt/5.15.2/android

cmake -GNinja ../ \
    -DCMAKE_BUILD_TYPE=Release \
    -DANDROID_ABI=armeabi-v7a \
    -DANDROID_BUILD_ABI_armeabi-v7a=ON \
    -DANDROID_BUILD_ABI_arm64-v8a:BOOL=ON \
    -DANDROID_BUILD_ABI_x86:BOOL=ON \
    -DANDROID_BUILD_ABI_x86_64:BOOL=ON \
    -DANDROID_NATIVE_API_LEVEL=$ANDROID_API \
    -DCMAKE_FIND_ROOT_PATH=$CMAKE_PREFIX_PATH \
    -DCMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH \
    -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake \
    -DANDROID_STL=c++_shared \
    -DANDROID_TOOLCHAIN=clang \
    -DANDROID_SDK=$ANDROID_HOME \
    -DCMAKE_ANDROID_NDK=$ANDROID_NDK \
    -DCMAKE_INSTALL_PREFIX=./install


# Build the package
#ninja aab
#ninja apk


# Instal to the phone
#adb install -r android-build/build/outputs/apk/debug/android-build-debug.apk


# Start for some reason we need the sleep here
#sleep 1
#adb shell am start -S -W -n org.sars.virtualsolitaire/org.qtproject.qt5.android.bindings.QtActivity

# sign the bundle
# jarsigner --verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore <key> -signedjar <out.aab> android-build/build/outputs/bundle/release/android-build-release.aab "<cert alias>"

