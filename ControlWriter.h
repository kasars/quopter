#ifndef ControlWriter_H
#define ControlWriter_H

#include <QObject>

class ControlWriter : public QObject
{
    Q_OBJECT

    Q_PROPERTY(float uplift MEMBER m_uplift NOTIFY upliftChanged)
    Q_PROPERTY(float yaw    MEMBER m_yaw    NOTIFY yawChanged)
    Q_PROPERTY(float pitch  MEMBER m_pitch  NOTIFY pitchChanged)
    Q_PROPERTY(float roll   MEMBER m_roll   NOTIFY rollChanged)

    Q_PROPERTY(float calYaw    READ calYaw    NOTIFY calYawChanged)
    Q_PROPERTY(float calPitch  READ calPitch  NOTIFY calPitchChanged)
    Q_PROPERTY(float calRoll   READ calRoll   NOTIFY calRollChanged)

    Q_PROPERTY(bool armPressed    READ armPressed     NOTIFY armChanged)
    Q_PROPERTY(bool disarmPressed READ disarmPressed  NOTIFY disarmChanged)
    Q_PROPERTY(bool landPressed   READ landPressed    NOTIFY landChanged)
    Q_PROPERTY(bool calibrPressed READ calibrPressed  NOTIFY calibrChanged)

    Q_PROPERTY(bool autoHeight    MEMBER m_autoHeight NOTIFY autoHeightChanged)

public:
    explicit ControlWriter(QObject *parent = nullptr);
    ~ControlWriter();

    float calYaw() const;    // Calibration -1 -> 1
    float calPitch() const;
    float calRoll() const;

    Q_INVOKABLE void calYawAdd();
    Q_INVOKABLE void calYawDec();

    Q_INVOKABLE void calPitchAdd();
    Q_INVOKABLE void calPitchDec();

    Q_INVOKABLE void calRollAdd();
    Q_INVOKABLE void calRollDec();

    bool armPressed() const;
    bool disarmPressed() const;
    bool landPressed() const;
    bool calibrPressed() const;

    Q_INVOKABLE void handleArm();
    Q_INVOKABLE void handleDisarm();
    Q_INVOKABLE void handleLand();

    Q_INVOKABLE void handleCalibrate();

    Q_INVOKABLE void toggleRev();

// public Q_SLOTS:
//     void handleAutoHeight();

Q_SIGNALS:
    void upliftChanged();
    void yawChanged();
    void pitchChanged();
    void rollChanged();

    void calYawChanged();
    void calPitchChanged();
    void calRollChanged();

    void armChanged();
    void disarmChanged();
    void landChanged();
    void calibrChanged();
    void autoHeightChanged();

private:
    float m_uplift = 0;
    float m_yaw = 0;
    float m_pitch = 0;
    float m_roll = 0;

    bool m_autoHeight = true;

    class Private;
    friend class Private;
    Private *const d;
};

#endif // UDPREADER_H
