#ifndef MyUDPReader_H
#define MyUDPReader_H

#include <QObject>
#include <QUdpSocket>

class MyUDPReader : public QObject
{
    Q_OBJECT

public:
    explicit MyUDPReader(QObject *parent = nullptr);

private Q_SLOTS:
    void readData();

private:
    QUdpSocket readSocket;

};

#endif // UDPREADER_H
