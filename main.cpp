/* ============================================================
 *
 * Copyright (C) 2019 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "ControlWriter.h"

// #include "SpyProxy.h"
// #include "MyUDPReader.h"

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QtAndroid>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QtAndroidExtras/QAndroidJniObject>
void keep_screen_on(bool on) {
    QtAndroid::runOnAndroidThread([on]{
        QAndroidJniObject activity = QtAndroid::androidActivity();
        if (activity.isValid()) {
            QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");

            if (window.isValid()) {
                const int FLAG_KEEP_SCREEN_ON = 128;
                if (on) {
                    window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
                } else {
                    window.callMethod<void>("clearFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
                }
            }
        }
        QAndroidJniEnvironment env;
        if (env->ExceptionCheck()) { env->ExceptionClear(); }
    });
}

#else
void keep_screen_on(bool) {}
#endif


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    ControlWriter ctrlWriter;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("ctrlWriter", &ctrlWriter);
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

//     SpyProxy spy;
//     MyUDPReader reader;

    keep_screen_on(true);

    return app.exec();
}
