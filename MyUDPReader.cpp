#include "MyUDPReader.h"
#include <QDebug>

MyUDPReader::MyUDPReader(QObject *parent) : QObject(parent)
{
    qDebug() << "MyUDPReader";

    connect(&readSocket, &QUdpSocket::readyRead, this, &MyUDPReader::readData);

    bool bindOK = readSocket.bind(QHostAddress::Any, 8080, QUdpSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);
    qDebug() << "bind:" << (bindOK ? "OK" : "Failed");
}


void MyUDPReader::readData()
{
    QByteArray datagram;
    QHostAddress senderAddr;
    quint16 port = 0;

    while (readSocket.hasPendingDatagrams()) {
        datagram.resize((int)readSocket.pendingDatagramSize());
        readSocket.readDatagram(datagram.data(), datagram.size(), &senderAddr, &port);
        if ((unsigned char)datagram[0] == 0xFF) {
            qDebug() << senderAddr << port << ":" << datagram.toHex(' ');
        }
        else if ((unsigned char)datagram[0] == 0x0F) {
            qDebug() << senderAddr << port << ":" << datagram.toHex(' ');
        }
        else if ((unsigned char)datagram[0] == 0x28) {
            qDebug() << senderAddr << port << ":" << datagram.toHex(' ');
        }
        else {
            qDebug() << senderAddr << port << ":----:" << datagram.toHex(' ');
        }
    }

}


