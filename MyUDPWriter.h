#ifndef MyUDPWriter_H
#define MyUDPWriter_H

#include <QObject>
#include <QUdpSocket>
#include <QTimer>

class MyUDPWriter : public QObject
{
    Q_OBJECT

public:
    explicit MyUDPWriter(QObject *parent = nullptr);

private Q_SLOTS:
    void writeData();
    void readData();

private:
    QUdpSocket m_writeSocket;
    QTimer     m_timer;

};

#endif // UDPREADER_H
