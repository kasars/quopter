#include "ControlWriter.h"
#include <QDebug>
#include <QUdpSocket>
#include <QTimer>
#include <QPointer>
#include <QNetworkInterface>

/*
 *  Commands on UDP port 8080
 *  byte 0 == command
 *    No parameters
 *      0x01 toggle Rev on -> resp: "ok\x00"
 *      0x02 toggle Fev off -> resp: "ok\x00"
 *      0x0F request video format -> resp: "VSVGA\x00"
 *      0x28 Request version information? -> resp: "V2.66-N\x00"
 *      0x2C ??? 1-2 times (B,BBBB)
 *      0x42 ??? 3-5 times (B,BBBB)
 *    With parameters
 *      0xFF is used for sending steering commands
 *         len (0x08)
 *         throttle (height) 0x00 -> 0xFC, 73 == center
 *         rotate 0x00 -> 0x7E, 0x3F == center
 *         fwd/bkw 0x7F == full back, 0x01 == full forward, 0x40 == center
 *         roll left-right full left = 0x00, center 0x3f, full right = 0x7E
 *         calibrate rotate l-r bits 0-5 0 = left 0x20 right center = 0x10 + 0x80 = "auto height" + enable controls + 0x40 calibrate gyro
 *         calibrate fwd/rew: rew 0x00, fwd = 0x20, center 0x10  + no status bits?
 *         calibrate rol l/R 0x00 = left, 0x20 = right, center 0x10  + no status bits?
 *         command flags arm 0x40, down = 0x80, stop 0x20, bits 0&1 sensitivity? 0 = 30%, 1 60% 3 = 100%, 4 = Flip mode (activate on pad movement)
 *         CRC = 0xFF - 0x08 - uplift -yaw - ... - ... flags
 *
 */


static const float calStep = 1.0/0x10;

unsigned char ratioToUchar(float min, float max, float ratio)
{
    return (unsigned char)(min + (max - min) * (ratio + 1.0) / 2);
}

class ControlWriter::Private {
public:
    Private(ControlWriter *pub): q(pub) {}

    ControlWriter *q;
    QTimer m_armTimer;
    QTimer m_disarmTimer;
    QTimer m_landTimer;
    QTimer m_calibrTimer;

    QTimer m_cmdTimer;
    QTimer m_infoTimer;

    QString m_version;
    QString m_videoFormat;

    bool m_armPressed = false;
    bool m_disarmPressed = false;
    bool m_landPressed = false;

    bool m_rev = false;

    float m_calYaw = 0.0;
    float m_calPitch = 0.0;
    float m_calRoll = 0.0;

    unsigned char m_flags = 0;

    QUdpSocket m_droneSocket;
    QHostAddress m_droneAddr = QHostAddress("192.168.1.1");

    void writeInfoReq() {
        if (!m_version.isEmpty() && !m_version.isEmpty()) {
            m_infoTimer.stop();
            return;
        }

        if (m_videoFormat.isEmpty()) {
            QByteArray datagram;
            datagram += 0x0F;
            //qDebug() << datagram.toHex(' ');
            m_droneSocket.writeDatagram(datagram, m_droneAddr, 8080);
        }

        if (m_version.isEmpty()) {
            QByteArray datagram;
            datagram += 0x28;
            //qDebug() << datagram.toHex(' ');
            m_droneSocket.writeDatagram(datagram, m_droneAddr, 8080);
        }
    }

    void writeCmd() {
        QByteArray datagram(11, 0);
        unsigned char crc = 0xFF;
        unsigned char tmp = 0;

        datagram[0] = 0xFF;
        crc = 0xFF;
        datagram[1] = 0x08;
        crc -= 0x08;

        unsigned char flags = m_flags;
        if (m_landTimer.isActive())   { flags |= 0x80; }
        if (m_armTimer.isActive())    { flags |= 0x40; }
        if (m_disarmTimer.isActive()) { flags |= 0x20; }

        // Uplift / Downfall 0x00 -> 0xFC, 7E == center
        tmp = ratioToUchar(0x00, 0xFC, q->m_uplift);
        crc -= tmp;
        datagram[2] = tmp;

        //  Yaw (rotate) 0x00 -> 0x7E, 0x3F == center
        tmp = ratioToUchar(0x00, 0x7E, q->m_yaw);
        crc -= tmp;
        datagram[3] = tmp;

        //  fwd/bkw 0x7F == full back, 0x01 == full forward, 0x40 == center
        tmp = ratioToUchar(0x7F, 0x01, q->m_pitch);
        crc -= tmp;
        datagram[4] = tmp;

        //  roll left-right full left = 0x00, center 0x3f, full right = 0x7E
        tmp = ratioToUchar(0x00, 0x7E, q->m_roll);
        crc -= tmp;
        datagram[5] = tmp;

        //  calibrate rotate l-r bits 0-5 0 = left 0x20 right center = 0x10 + MSB = "auto height" + enable controls
        tmp = ratioToUchar(0x00, 0x20, m_calYaw);
        if (q->m_autoHeight)  { tmp |= 0x80; }
        if (m_calibrTimer.isActive()) { tmp |= 0x40; }
        crc -= tmp;
        datagram[6] = tmp;

        //  calibrate fwd/rew: rew 0x00, fwd = 0x20, center 0x10  + no status bits?
        tmp = ratioToUchar(0x00, 0x20, m_calPitch);
        crc -= tmp;
        datagram[7] = tmp;

        //  calibrate roll l/R 0x00 = left, 0x20 = right, center 0x10  + no status bits?
        tmp = ratioToUchar(0x00, 0x20, m_calRoll);
        crc -= tmp;
        datagram[8] = tmp;

        //  command flags arm 0x40, down = 0x80, stop 0x20, bits 0&1 sensitivity? 0 = 30%, 1 60% 3 = 100%
        crc -= flags;
        datagram[9] = flags;

        // CRC...
        datagram[10] = crc;

        //qDebug() << datagram.toHex(' ');
        m_droneSocket.writeDatagram(datagram, m_droneAddr, 8080);
    }

    void readData() {
        QByteArray datagram;
        QHostAddress senderAddr;
        quint16 port = 0;

        while (m_droneSocket.hasPendingDatagrams()) {
            datagram.resize((int)m_droneSocket.pendingDatagramSize());
            m_droneSocket.readDatagram(datagram.data(), datagram.size(), &senderAddr, &port);
            qDebug() << datagram.size() << datagram.toHex(' ') << datagram;

            if (datagram[0] == 'V') {
                if (datagram[1] == '2') {
                    m_version = datagram;
                }
                else {
                    m_videoFormat = datagram;
                }
            }
        }
    }
};

ControlWriter::ControlWriter(QObject *parent) : QObject(parent), d(new ControlWriter::Private(this))
{
    qDebug() << "ControlWriter";

    d->m_armTimer.setInterval(1000);
    d->m_armTimer.setSingleShot(true);

    d->m_disarmTimer.setInterval(1000);
    d->m_disarmTimer.setSingleShot(true);

    d->m_landTimer.setInterval(1000);
    d->m_landTimer.setSingleShot(true);

    d->m_calibrTimer.setInterval(1000);
    d->m_calibrTimer.setSingleShot(true);

    d->m_cmdTimer.setInterval(50);
    d->m_cmdTimer.setSingleShot(false);

    d->m_infoTimer.setInterval(200);
    d->m_infoTimer.setSingleShot(false);

    connect(&d->m_droneSocket, &QUdpSocket::readyRead, this, [this]() { d->readData(); });
    d->m_droneSocket.bind(QHostAddress::Any, 23174, QUdpSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);

    connect(&d->m_cmdTimer, &QTimer::timeout, this, [this]() { d->writeCmd(); });

    connect(&d->m_infoTimer, &QTimer::timeout, this, [this]() { d->writeInfoReq(); });


    connect(&d->m_armTimer, &QTimer::timeout, this, &ControlWriter::armChanged);
    connect(&d->m_disarmTimer, &QTimer::timeout, this, &ControlWriter::disarmChanged);
    connect(&d->m_landTimer, &QTimer::timeout, this, &ControlWriter::landChanged);
    connect(&d->m_calibrTimer, &QTimer::timeout, this, &ControlWriter::calibrChanged);

    d->m_infoTimer.start();
    d->m_cmdTimer.start();
}

ControlWriter::~ControlWriter()
{
    delete d;
}

float ControlWriter::calYaw() const   { return d->m_calYaw; }
float ControlWriter::calPitch() const { return d->m_calPitch; }
float ControlWriter::calRoll() const  { return d->m_calRoll; }

void ControlWriter::calYawAdd() { if (d->m_calYaw < 1.0) d->m_calYaw += calStep; }
void ControlWriter::calYawDec() { if (d->m_calYaw > -1.0) d->m_calYaw -= calStep; }

void ControlWriter::calPitchAdd() { if (d->m_calPitch < 1.0) d->m_calPitch += calStep; }
void ControlWriter::calPitchDec() { if (d->m_calPitch > -1.0) d->m_calPitch -= calStep; }

void ControlWriter::calRollAdd() { if (d->m_calRoll < 1.0) d->m_calRoll += calStep; }
void ControlWriter::calRollDec() { if (d->m_calRoll > -1.0) d->m_calRoll -= calStep; }


bool ControlWriter::armPressed() const    { return d->m_armTimer.isActive(); }
bool ControlWriter::disarmPressed() const { return d->m_disarmTimer.isActive(); }
bool ControlWriter::landPressed() const   { return d->m_landTimer.isActive(); }
bool ControlWriter::calibrPressed() const { return d->m_calibrTimer.isActive(); }


void ControlWriter::handleArm()    { d->m_disarmTimer.stop(); d->m_armTimer.start(); emit armChanged(); emit disarmChanged(); }
void ControlWriter::handleDisarm() { d->m_armTimer.stop(); d->m_disarmTimer.start(); emit armChanged(); emit disarmChanged(); }
void ControlWriter::handleLand()   { d->m_landTimer.start();  emit landChanged(); }
void ControlWriter::handleCalibrate() { d->m_calibrTimer.start();  emit calibrChanged(); }

// void ControlWriter::handleAutoHeight()
// {
//     qDebug() << "autoheight:" << m_autoHeight;
// }


void ControlWriter::toggleRev()
{
    if (!d->m_rev) {
        QByteArray datagram;
        datagram += 0x01;
        //qDebug() << datagram.toHex(' ');
        d->m_droneSocket.writeDatagram(datagram, d->m_droneAddr, 8080);
        d->m_rev = true;
    }
    else {
        QByteArray datagram;
        datagram += 0x02;
        //qDebug() << datagram.toHex(' ');
        d->m_droneSocket.writeDatagram(datagram, d->m_droneAddr, 8080);
        d->m_rev = false;
    }

}


