#include "MyUDPWriter.h"
#include <QDebug>
#include <QNetworkInterface>

MyUDPWriter::MyUDPWriter(QObject *parent) : QObject(parent)
{
    qDebug() << "MyUDPWriter";

    m_timer.setInterval(200);
    m_timer.setSingleShot(false);

    connect(&m_writeSocket, &QUdpSocket::readyRead, this, &MyUDPWriter::readData);
    m_writeSocket.bind(QHostAddress::Any, 23174, QUdpSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);


    connect(&m_timer, &QTimer::timeout, this, &MyUDPWriter::writeData);

    m_timer.start();
}


void MyUDPWriter::writeData()
{
    QByteArray datagram;
    QHostAddress senderAddr;

    for (const QNetworkInterface &interf: QNetworkInterface::allInterfaces()) {
        if (interf.type() == QNetworkInterface::Loopback) {
            continue;
        }
        for (const QNetworkAddressEntry &addr: interf.addressEntries()) {
            if (addr.ip().protocol() != QAbstractSocket::IPv4Protocol) {
                continue;
            }
            quint32 intaddr = addr.ip().toIPv4Address();

            qDebug() << QString::number(intaddr, 16);
            intaddr &= 0xFFFFFF00;
            intaddr |= 0x01;
            qDebug() << QString::number(intaddr, 16) << QHostAddress(intaddr);
            QHostAddress copterAddr(intaddr);

            datagram += 0x0F;
            m_writeSocket.writeDatagram(datagram, copterAddr, 8080);

            datagram[0] = 0x28;
            m_writeSocket.writeDatagram(datagram, copterAddr, 8080);
        }
    }
}

void MyUDPWriter::readData()
{
    QByteArray datagram;
    QHostAddress senderAddr;
    quint16 port = 0;

    while (m_writeSocket.hasPendingDatagrams()) {
        datagram.resize((int)m_writeSocket.pendingDatagramSize());
        m_writeSocket.readDatagram(datagram.data(), datagram.size(), &senderAddr, &port);
        if ((unsigned char)datagram[0] == 0xFF) {
            //qDebug() << senderAddr << port << ":" << datagram.toHex(' ');
        }
        else if ((unsigned char)datagram[0] == 0x0F) {
            qDebug() << senderAddr << port << ":" << datagram.toHex(' ');
        }
        else if ((unsigned char)datagram[0] == 0x28) {
            qDebug() << senderAddr << port << ":" << datagram.toHex(' ');
        }
        else {
            qDebug() << senderAddr << port << ":----:" << datagram << datagram.toHex(' ');
        }
    }

}

